﻿$(function () {
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container2',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 30,
                depth: 50,
                viewDistance: 25
            },
            //events: {
            //    load: function () {
            //        var count = 0;
            //        setInterval(function () {
            //            if (count == 0) {
            //                chart.series[0].setData([1, 5, 7, 1]);
            //                //chart.series[1].setData([10.57]);
            //                //chart.series[2].setData([7.23]);
            //                count = 1;
            //            }
            //            else {
            //                chart.series[0].setData([0, 0, 0, 0]);
            //                //chart.series[1].setData([0]);
            //                //chart.series[2].setData([0]);
            //                count = 0;
            //            }
            //        }, 1600);
            //    }
            //}
        },
        title: {
            text: 'BÁO CÁO 115',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['dịch bệnh', 'ATVSTP', 'ngộ độc', 'sự cố BV']
        },
        yAxis: {
            max:8,
            title: {
                text: 'Số vụ'
            }
        },
        colors: ['#00ffff', '#ffc107', '#cddc39', '#e91e63'],

        plotOptions: {
            column: {
                depth: 40,
                colorByPoint: true
            }
        },
        series: [{
            showInLegend: false,
            data: [1, 5, 7, 1]
        },
        ],
        credits: {
            enabled: false
        },
        exporting: { enabled: false }
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

    showValues();
});