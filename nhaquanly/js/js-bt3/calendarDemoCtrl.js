angular.module('calendarDemoApp', ['ui.rCalendar']);

angular.module('calendarDemoApp').controller('CalendarDemoCtrl', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Theo dõi, ứng phó với siêu bão MangKhut",
                noidung: "Theo dõi, ứng phó với siêu bão MangKhut",
                thamdu: "TCPCTT*",
                diadiem: "",
                startTime: new Date(2019, 8, 17, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 17, 23, 59, 59, 0),
                allDay: false
            },
            {
                title: "Họp Ban cán sự ",
                noidung: "Họp Ban cán sự ",
                thamdu: "LĐ Bộ, VP BCS*, TK BT.",
                diadiem: "2_A2",
                startTime: new Date(2019, 8, 18, 14, 0, 0, 0),
                endTime: new Date(2019, 8, 18, 16, 0, 0, 0),
                allDay: false
            },
            {
                title: "Dự họp UBTVQH về giám sát chuyên đề và chất vấn tại kỳ họp, kết luận của UBTVQH về chất vấn tại phiên họp ",
                noidung: "Dự họp UBTVQH về giám sát chuyên đề và chất vấn tại kỳ họp, kết luận của UBTVQH về chất vấn tại phiên họp ",
                thamdu: "Vụ PC, VP Bộ*",
                diadiem: "VPQH ",
                startTime: new Date(2019, 8, 19, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 19, 13, 0, 0, 0),
                allDay: false
            },
            {
                title: "Họp UBTVQH về Luật sửa đổi, bổ sung các luật có quy định liên quan đến quy hoạch ",
                noidung: "Họp UBTVQH về Luật sửa đổi, bổ sung các luật có quy định liên quan đến quy hoạch ",
                thamdu: "Vụ PC, KH*, ",
                diadiem: "VPQH ",
                startTime: new Date(2019, 8, 19, 14, 0, 0, 0),
                endTime: new Date(2019, 8, 19, 17, 0, 0, 0),
                allDay: false
            },
            {
                title: " Đi công tác Tuyên Quang ",
                noidung: " Đi công tác Tuyên Quang ",
                thamdu: "VP Bộ chuẩn bị và mời, theo GM,",
                diadiem: "Tuyên Quang ",
                startTime: new Date(2019, 8, 20, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 20, 17, 0, 0, 0),
                allDay: false
            },
            {
                title: "Nghe Vụ HTQT báo cáo một số vấn đề liên quan ",
                noidung: "Nghe Vụ HTQT báo cáo một số vấn đề liên quan ",
                thamdu: "TT Hà Công Tuấn, Vụ HTQT*, TCTS*,  ",
                diadiem: "2_A2 ",
                startTime: new Date(2019, 8, 21, 13, 50, 0, 0),
                endTime: new Date(2019, 8, 21, 15, 0, 0, 0),
                allDay: false
            },
            {
                title: "Nghe báo cáo Demo giới thiệu trung tâmđiều hành thông minh ",
                noidung: "Nghe báo cáo Demo giới thiệu trung tâmđiều hành thông minh ",
                thamdu: "LĐ Bộ, VP Bộ*, VT các Vụ: KH, TC, KHCN, TCCB",
                diadiem: "tầng 19, tòa nhà Lotte 54 Liễu Giai.",
                startTime: new Date(2019, 8, 21, 15, 0, 0, 0),
                endTime: new Date(2019, 8, 21, 16, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end bộ trưởng  ----->

angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr1', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Làm việc với Đại sứ EU về VPA/FLEGT",
                noidung: "Làm việc với Đại sứ EU về VPA/FLEGT",
                thamdu: "TCLN*, Vụ HTQT,",
                diadiem: "7A2",
                startTime: new Date(2019, 8, 17, 8, 30, 0, 0),
                endTime: new Date(2019, 8, 17, 9, 30, 0, 0),
                allDay: false
            },
            {
                title: " Họp tư vấn về Tổng kết tam nông ",
                noidung: " Họp tư vấn về Tổng kết tam nông ",
                thamdu: "Vụ KH*.",
                diadiem: "2_A2 ",
                startTime: new Date(2019, 8, 18, 8, 30, 0, 0),
                endTime: new Date(2019, 8, 18, 11, 0, 0, 0),
                allDay: false
            },
            {
                title: "Họp Ban cán sự ",
                noidung: "Họp Ban cán sự ",
                thamdu: "Theo lịch Bộ trưởng,  ",
                diadiem: "2_A2 ",
                startTime: new Date(2019, 8, 18, 14, 0, 0, 0),
                endTime: new Date(2019, 8, 18, 15, 0, 0, 0),
                allDay: false
            },
            {
                title: " Họp Ban tổ chức Hội nghị sơ kết 5 năm tái cơ cấu ngành; Hội đồng giải thưởng Bông lúa vàng ",
                noidung: " Họp Ban tổ chức Hội nghị sơ kết 5 năm tái cơ cấu ngành; Hội đồng giải thưởng Bông lúa vàng ",
                thamdu: "Vụ TCCB*,  ",
                diadiem: "102_B6 ",
                startTime: new Date(2019, 8, 19, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 19, 12, 0, 0, 0),
                allDay: false
            },
            {
                title: "Tiếp Đại sứ quán Ba Lan ",
                noidung: "Tiếp Đại sứ quán Ba Lan ",
                thamdu: "Vụ HTQT*, Cục TY,  ",
                diadiem: "7A2 ",
                startTime: new Date(2019, 8, 19, 13, 30, 0, 0),
                endTime: new Date(2019, 8, 19, 14, 0, 0, 0),
                allDay: false
            },
            {
                title: "Làm việc với TCTS (hoãn)    ",
                noidung: "Làm việc với TCTS (hoãn)  ",
                thamdu: "TCTS* (mời cán bộ chủ chốt), Vụ TCCB, VP Bộ,  ",
                diadiem: "TCTS ",
                startTime: new Date(2019, 8, 19, 15, 0, 0, 0),
                endTime: new Date(2019, 8, 19, 16, 30, 0, 0),
                allDay: false
            },
            {
                title: "Đi công tác địa phương ",
                noidung: "Đi công tác địa phương ",
                thamdu: "VP Bộ*",
                diadiem: "",
                startTime: new Date(2019, 8, 20, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 20, 17, 0, 0, 0),
                allDay: false
            },
            {
                title: "Làm việc với Cục QLCL NLTS ",
                noidung: "Làm việc với Cục QLCL NLTS ",
                thamdu: "Cục QLCN NLTS*, Cục: TT, BVTV, CN, TY, CBTMNLS và Vụ KHCN&MT,  ",
                diadiem: "Cục QLCL NLTS ",
                startTime: new Date(2019, 8, 21, 8, 30, 0, 0),
                endTime: new Date(2019, 8, 21, 12, 0, 0, 0),
                allDay: false
            },
            {
                title: " Họp vói Vinafood I (Vụ trưởng Vụ QLDN Phạm Quang Hiển chủ trì) ",
                noidung:  "Họp vói Vinafood I (Vụ trưởng Vụ QLDN Phạm Quang Hiển chủ trì) ",
                thamdu: "Vụ QLDN*",
                diadiem: "101_B6 ",
                startTime: new Date(2019, 8, 21, 13, 30, 0, 0),
                endTime: new Date(2019, 8, 21, 13, 30, 0, 0),
                allDay: false
            }
            ,
            {
                title: "  Họp về thương mại điện tử nông sản ",
                noidung:  " Họp về thương mại điện tử nông sản ",
                thamdu: "Theo lịch Bộ trưởng,  ",
                diadiem: "2_A2 ",
                startTime: new Date(2019, 8, 21, 13, 30, 0, 0),
                endTime: new Date(2019, 8, 21, 13, 30, 0, 0),
                allDay: false
            }
            ,
            {
                title: " Họp về Chính phủ điện tử ",
                noidung:  "Theo lịch Bộ trưởng,   ",
                thamdu: "",
                diadiem: "",
                startTime: new Date(2019, 8, 21, 15, 30, 0, 0),
                endTime: new Date(2019, 8, 21, 16, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end thứ trưởng 1  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr2', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Theo dõi, ứng phó với siêu bão MangKhut",
                noidung: "Theo dõi, ứng phó với siêu bão MangKhut",
                thamdu: "",
                diadiem: "",
                startTime: new Date(2019, 8, 17, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 17, 23, 59, 0, 0),
                allDay: false
            },
            {
                title: "Dự Lễ khai giảng năm học mới Trường Đại học Thủy lợi",
                noidung: "Dự Lễ khai giảng năm học mới Trường Đại học Thủy lợi ",
                thamdu: "Theo giấy mời,  ",
                diadiem: "Theo giấy mời,  ",
                startTime: new Date(2019, 8, 17, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 17, 8, 30, 0, 0),
                allDay: false
            },
            {
                title: " Họp Chính phủ về các vướng mắc trong việc chuẩn bị các chương trình, dự án vay vốn COL (ADF) của Ngân hàng Phát triển Châu á (ADB) tài khóa 2019 ",
                noidung: " Họp Chính phủ về các vướng mắc trong việc chuẩn bị các chương trình, dự án vay vốn COL (ADF) của Ngân hàng Phát triển Châu á (ADB) tài khóa 2019 ",
                thamdu: "Vụ HTQT chuẩn bị và cùng dự,  ",
                diadiem: "VPCP ",
                startTime: new Date(2019, 8, 17, 10, 0, 0, 0),
                endTime: new Date(2019, 8, 17, 11, 30, 0, 0),
                allDay: false
            },
            {
                title: "Nghe báo cáo TKKT dự án tiêu úng vùng III Nông Cống, tỉnh Thanh Hóa ",
                noidung: "Nghe báo cáo TKKT dự án tiêu úng vùng III Nông Cống, tỉnh Thanh Hóa ",
                thamdu: "Vụ trưởng Vụ TCCB chuẩn bị và cùng dự.",
                diadiem: "Phòng khách A604",
                startTime: new Date(2019, 8, 17, 15, 0, 0, 0),
                endTime: new Date(2019, 8, 17, 16, 0, 0, 0),
                allDay: false
            },
            {
                title: "Tham dự phiên họp thứ 27 của UBTV Quốc hội về cho ý kiến về dự án Luật sửa đổi, bổ sung 1 số điều của Luật Đầu tư công ",
                noidung: "Tham dự phiên họp thứ 27 của UBTV Quốc hội về cho ý kiến về dự án Luật sửa đổi, bổ sung 1 số điều của Luật Đầu tư công ",
                thamdu: "Vụ KH chuẩn bị và cùng dự,  ",
                diadiem: "Nhà Quốc Hội ",
                startTime: new Date(2019, 8, 20, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 20, 10, 0, 0, 0),
                allDay: false
            },
            {
                title: "Dự họp Thường trực Chính phủ ",
                noidung: "Dự họp Thường trực Chính phủ ",
                thamdu: "Theo giấy mời,  ",
                diadiem: "VPCP ",
                startTime: new Date(2019, 8, 20, 14, 0, 0, 0),
                endTime: new Date(2019, 8, 20, 16, 0, 0, 0),
                allDay: false
            },
            {
                title: "Đồng chủ trì với Lãnh đạo UBND THÀNH PHỐ HÀ NỘI tại Hội nghị về công tác quản lý nước sạch nông thôn ",
                noidung: "Đồng chủ trì với Lãnh đạo UBND THÀNH PHỐ HÀ NỘI tại Hội nghị về công tác quản lý nước sạch nông thôn ",
                thamdu: "   Theo giấy mời,  ",
                diadiem: "Hà Nội ",
                startTime: new Date(2019, 8, 21, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 21, 17, 0, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);

// end thứ trưởng 2  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr3', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
                title: "Đi công tác nước ngoài ",
                noidung: "Đi công tác nước ngoài ",
                thamdu: "Vụ HTQT*, Viện CSCLNN&PTNT.,  ",
                diadiem: "Anh",
                startTime: new Date(2019, 8, 19, 8, 0, 0, 0),
                endTime: new Date(2019, 8, 19, 23, 59, 59, 0),
                allDay: false
            },
            {
                title: "Đi công tác nước ngoài ",
                noidung: "Đi công tác nước ngoài ",
                thamdu: "Vụ HTQT*, Viện CSCLNN&PTNT.,  ",
                diadiem: "Anh",
                startTime: new Date(2019, 8, 20, 0, 1, 0, 0),
                endTime: new Date(2019, 8, 20, 23, 59, 0, 0),
                allDay: false
            },
            {
                title: "Đi công tác nước ngoài ",
                noidung: "Đi công tác nước ngoài ",
                thamdu: "Vụ HTQT*, Viện CSCLNN&PTNT.,  ",
                diadiem: "Anh",
                startTime: new Date(2019, 8, 21, 0, 1, 0, 0),
                endTime: new Date(2019, 8, 21, 23, 59, 0, 0),
                allDay: false
            }
        ];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);


// end thứ trưởng 3  ----->
angular.module('calendarDemoApp').controller('CalendarDemoCtrl_ttr4', ['$scope', function($scope) {
    'use strict';
    $scope.changeMode = function(mode) {
        $scope.mode = mode;
    };

    $scope.today = function() {
        $scope.currentDate = new Date();
    };

    $scope.isToday = function() {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function() {
        var date = new Date();
        $scope.eventSource = [{
            title: " Họp công tác chuẩn bị hội nghị ổn định dân di cư tự do ",
            noidung: " Họp công tác chuẩn bị hội nghị ổn định dân di cư tự do ",
            thamdu: "Cục KTHT,  ",
            diadiem: "218-A10 ",
            startTime: new Date(2019, 8, 20, 8, 30, 0, 0),
            endTime: new Date(2019, 8, 20, 9, 45, 0, 0),
            allDay: false
        },
        {
            title: "Làm việc với Ban chủ nhiệm Chương trình KHCN NTM ",
            noidung: "Làm việc với Ban chủ nhiệm Chương trình KHCN NTM ",
            thamdu: "VPĐP*,  ",
            diadiem: "102-B6 ",
            startTime: new Date(2019, 8, 20, 14, 0, 0, 0),
            endTime: new Date(2019, 8, 20, 14, 30, 0, 0),
            allDay: false
        },
        {
            title: "Hội nghị trực tuyến Chính phủ Tổng kết Phong trào Toàn dân đoàn kết xây dựng đời sống văn hóa ",
            noidung: "Hội nghị trực tuyến Chính phủ Tổng kết Phong trào Toàn dân đoàn kết xây dựng đời sống văn hóa ",
            thamdu: "VPĐP,  ",
            diadiem: "11 Lê Hồng Phong ",
            startTime: new Date(2019, 8, 21, 8, 0, 0, 0),
            endTime: new Date(2019, 8, 21, 11, 0, 0, 0),
            allDay: false
        },
        {
            title: "Làm việc Tổ chức IFOAM, Hiệp hội nông nghiệp hữu cơ ",
            noidung: "Làm việc Tổ chức IFOAM, Hiệp hội nông nghiệp hữu cơ ",
            thamdu: " ",
            diadiem: "218-A10 ",
            startTime: new Date(2019, 8, 21, 14, 30, 0, 0),
            endTime: new Date(2019, 8, 21, 15, 0, 0, 0),
            allDay: false
        },];

        console.log($scope.eventSource);
    };

    $scope.onEventSelected = function(event) {
        console.log(event);
        $scope.event = event;
    };

    $scope.onTimeSelected = function(selectedTime, events) {

        console.log(events);
        //console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
    };

    $scope.onclick = function(event) {
        console.log(event);
    }

    function createRandomEvents() {
        var events = [];
        for (var i = 0; i < 50; i += 1) {
            var date = new Date();
            var eventType = Math.floor(Math.random() * 2);
            var startDay = Math.floor(Math.random() * 90) - 45;
            var endDay = Math.floor(Math.random() * 2) + startDay;
            var startTime;
            var endTime;
            if (eventType === 0) {
                startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
                if (endDay === startDay) {
                    endDay += 1;
                }
                endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
                events.push({
                    title: 'All Day - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: true
                });
            } else {
                var startMinute = Math.floor(Math.random() * 24 * 60);
                var endMinute = Math.floor(Math.random() * 180) + startMinute;
                startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
                endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
                events.push({
                    title: 'Event - ' + i,
                    startTime: startTime,
                    endTime: endTime,
                    allDay: false
                });
            }

            //console.log(startTime);
            //console.log(endTime);
        }
        return events;
    }
}]);