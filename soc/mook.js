﻿var mook = {
    sobn: function () {
        let raw = `Sở Công Thương
Sở Nông nghiệp và PTNT
Sở Giao Thông Vận Tải
Sở Tài Chính
Sở Giáo dục và Đào tạo
Sở Tài nguyên và Môi trường
Sở Kế hoạch và Đầu tư
Sở Thông tin và Truyền thông
Sở Khoa học và Công nghệ
Sở Tư Pháp
Sở Lao động TB và Xã hội
Sở Văn hóa - Thể thao và Du lịch
Sở Nội Vụ
Sở Xây Dựng
Sở Y tế
Văn phòng UBND tỉnh
Thanh Tra tỉnh
BQL An toàn thực phẩm
BQL Khu công nghiệp
Trung tâm Hành chính công tỉnh
Viện NC phát triển Kinh tế - Xã hội`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `Tỉnh Ủy
Hội đồng Nhân dân Tỉnh`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = ` Quận Ba Đình‎
    Huyện Ba Vì‎
    Quận Cầu Giấy‎
    ​Huyện Chương Mỹ‎
    Huyện Đan Phượng‎
    Huyện Đông Anh
    Quận Đống Đa‎`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `Lực lượng CS PCCC
Lực Lượng phản ứng nhanh
Lực lượng y tế
Phòng tổng hợp
Phòng kế hoạch tài chính
Phòng điều phối
Lực lượng khác`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = ``;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
