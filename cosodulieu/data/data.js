﻿$(function () {
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'BIỂU ĐỒ THỐNG KÊ'
    },
    subtitle: {
        text: null
    },
    xAxis: {
        categories: ['TN Giao thông', 'Cháy KCN', 'Ngập úng', 'Cháy nhà dân'],
        title: {
            text: null
        }
    },
    credits: {
        enabled: false
    },
    exporting: { enabled: false },
    yAxis: {
        min: 0,
        title: {
            text: 'Số vụ',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' số vụ'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 40,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: '2014',
        data: [175, 168, 190, 168]
    }, {
        name: '2015',
        data: [3, 9, 10, 8]
    }, {
        name: '2016',
        data: [18, 13, 22, 16]
    }, {
        name: '2017',
        data: [80, 200, 280, 78]
    }]
});
});