﻿$(function () {
    Highcharts.chart('tngt', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Average Rainfall'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: [
              'Lực lượng công an',
              'Cảnh sát PCCC',
              'Đội ngũ Y tế',
              'Lực lượng giao thông'
            ],
            crosshair: true
        },
        yAxis: {
            max: 100,
            title: {
                text: null
            }
        },
        title: {
            text: 'KẾT QUẢ GIẢI QUYẾT Ý KIẾN PHẢN ÁNH CỦA NGƯỜI DÂN',
            style: {
                color: '#000000',
                fontWeight: 'bold'
            }
        },
        subtitle: {
            text: null
        },
        credits: {
            enabled: false
        },
        colors: ['green', 'orange'],
        exporting: { enabled: false },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.1f} sự cố</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Đã giải quyết',
            data: [88, 95, 82, 85]

        }, {
            name: 'Chưa giải quyết',
            data: [12, 5, 18, 15]

        }]
    });
});